#A metric measured in 2016 by asking the sampled people the question: "How would you rate your happiness on a scale of 0 to 10 where 10 is the happiest"
#Lower Confidence IntervalLower Confidence Interval of the Happiness Score
#Upper Confidence IntervalUpper Confidence Interval of the Happiness Score
#Economy (GDP per Capita)The extent to which GDP contributes to the calculation of the Happiness Score.
#FamilyThe extent to which Family contributes to the calculation of the Happiness Score
#Health (Life Expectancy)The extent to which Life expectancy contributed to the calculation of the Happiness Score
#FreedomThe extent to which Freedom contributed to the calculation of the Happiness Score
#Trust (Government Corruption)The extent to which Perception of Corruption contributes to Happiness Score
#GenerosityThe extent to which Generosity contributed to the calculation of the Happiness Score
#Dystopia ResidualThe extent to which Dystopia Residual contributed to the calculation of the Happiness Score.



#Componentes principales
#install.packages ("corrplot")
#install.packages ("FactoMineR")
#install.packages ("factoextra")
#install.packages ("mice")
#install.packages("sjPlot");



library(dplyr) # manipular los datos
library(sjPlot) #tablas cruzadas
library(readxl) #importar excel
library(lubridate) #manipular fechas
library(ggplot2)
library("FactoMineR")
library("factoextra")
library("mice") # Checking for missing variables
library("corrplot") # For the PCA graphs




#setear el directorio para realizar componentes principales

#setwd("/home/rstudio/analisis-inteligente/Clase VII-20190803");

#Abrir el archivo xlsx

felicidad_2017 <- read_excel("2017.xlsx")

act_col <- c(1,3, 6:11) 
feliz_new <- felicidad_2017[, act_col] 
colnames(feliz_new)[2]<-"Felicidad"
colnames(feliz_new)[3]<-"GDA_percapita"
colnames(feliz_new)[4]<-"Familia"
colnames(feliz_new)[5]<-"Esperanza_vida"
colnames(feliz_new)[6]<-"Libertad"
colnames(feliz_new)[7]<-"Generosidad"
colnames(feliz_new)[8]<-"Corrupcion"
rownames(feliz_new)<-feliz_new[,1] 

md.pattern(feliz_new)
summary(feliz_new)

M <- cor(feliz_new[,2:8]) 
corrplot(M, method = "ellipse")

pca_feliz <- prcomp(feliz_new[,2:8], scale=TRUE)
pca_feliz
summary(pca_feliz)

head(pca_feliz$sdev)

pca_feliz
#medias
pca_feliz$center

#desviacion
pca_feliz$scale

# autovalores
pca_feliz$sdev^2

#autovectores
pca_feliz$x

#autovectores
pca_feliz$rotation

pred <- predict(pca_feliz, newdata=feliz_new[,2:8])
feliz_new2 <- cbind(feliz_new, pred);

# mostrar que las nuevas variables son no correlacionadas
pred_corr <- cor(pred) 
corrplot(pred_corr, method = "ellipse")

plot(x=prcomp(feliz_new[,2:8]))
summary(prcomp(feliz_new[,2:8], scale = TRUE))

biplot(prcomp(feliz_new[,2:8], scale = TRUE))


biplot(pca_feliz,  scale = 0, cex = 0.6, col = c("blue4", "brown3"))