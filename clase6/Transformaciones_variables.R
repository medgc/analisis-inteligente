#limpio la memoria
rm( list=ls() )
gc()



install.packages ("Hmisc")
install.packages ("classInt")
install.packages ("questionr")

library(dplyr) #cuartiles
library(readxl) # leer archivos excel
library(Hmisc)
library(grid)
library(gridExtra)
library(classInt)
library(questionr)


#setwd("/home/rstudio/analisis-inteligente/clase5")


# TRansformaciones de variables

salarios <- read_excel("~/analisis-inteligente/clase6/Salarios.xls" )
summary(salarios)
freq.na(salarios)
head(salarios)

#Variables cualitativas a numericas (dummies)

salarios <-mutate(salarios, estadocivil = as.factor(estadocivil),
                 ocupacion = as.factor(ocupacion),
                 sexo=as.factor(sexo))


contrasts(salarios$ocupacion)
contrasts(salarios$sexo) 
contrasts(salarios$estadocivil) 


summary(salarios)

#Variables cuantitativas

#Discretizar una variable
#Particionamiento seg?n Igual tama?o 


clases <- classIntervals(salarios$ingresos, 4, style="quantile")
names(clases)
clases

salarios <- mutate(salarios, cut(salarios$ingresos, include.lowest=TRUE, breaks = clases$brks, labels =as.character(1:4)))

colnames(salarios)[9]<-"ingres_4"
freq <-freq(salarios$ingres_4)

#Particionamiento seg?n Igual ancho 
clases2 <- classIntervals(salarios$ingresos, 6, style="equal")

salarios <- mutate(salarios, cut(salarios$ingresos, breaks = clases2$brks, labels=as.character(1:6)))
colnames(salarios)[10]<-"ingres_6"
freq6 <-freq(salarios$ingres_6)

clases2

#Particion arbitraria

salarios <- mutate(salarios, ingres5 = as.character(ifelse(ingresos > 130000 , ">130000" , 
                                                        ifelse(ingresos > 99999, "100000 a 130000",
                                                               ifelse(ingresos > 74999, "75000 a 300000",
                                                                      ifelse(ingresos > 0, "0 a 75000", ingresos))))))
freq5 <-freq(salarios$ingres5)                                                              


# transformar distribuci?n de la variable
salarios  <- mutate(salarios, ingresos_trans = sqrt(ingresos))

# histograma
plot1 <- qplot(salarios$ingresos, xlab="Ingresos (en pesos)")
plot2 <- qplot(salarios$ingresos_trans, xlab="Ingresos transformados (ra?z cuadrada)")

grid.arrange(plot1, plot2, ncol=2)


#Metodos de estandarizaci?n 
#Normalizaci?n mix-max

salarios  <- mutate(salarios, ingresos_norm = (ingresos-min(ingresos))/(max(ingresos)-min(ingresos)))

# histograma
plot3 <- qplot(salarios$ingresos, xlab="Ingresos (en pesos)")
plot4 <- qplot(salarios$ingresos_norm, xlab="Ingresos estandarizados (mix-max)")

grid.arrange(plot3, plot4, ncol=2)

#Puntuaci?n z

salarios  <- mutate(salarios, ingresos_z = (ingresos-mean(ingresos))/(sd(ingresos)))

plot5 <- qplot(salarios$ingresos, xlab="Ingresos (en pesos)")
plot6 <- qplot(salarios$ingresos_z, xlab="Ingresos estandarizados (puntuaci?n z)")

grid.arrange(plot5, plot6, ncol=2)

